import os, shutil
from time import perf_counter
from statistics import mean, median, stdev
from pathlib import Path

user = Path.home()
source = user / "Downloads" / "Telegram Desktop"

dest = Path("D:") / "Alola Region"
dest1 = dest / "Sun and Moon"
dest2 = dest / "Sun and Moon Ultra Adventures"
dest3 = dest / "Sun and Moon Ultra Legends"

file_count = 0
times = []

for f in os.listdir(source):
	init = perf_counter()
	fpath = source / f
	appended = False
	destination = None

	try:
		if f.startswith('S20'):
			print(f"Moving {f} to {dest1}...")
			destination = dest1 / f
			shutil.move(fpath, dest1)
			file_count += 1
			appended = True

		elif f.startswith('S21'):
			print(f"Moving {f} to {dest2}...")
			destination = dest2 / f
			shutil.move(fpath, dest2)
			file_count += 1
			appended = True

		elif f.startswith('S22'):
			print(f"Moving {f} to {dest3}...")
			destination = dest3 / f
			shutil.move(fpath, dest3)
			file_count += 1
			appended = True

		final = perf_counter()
		if appended: times.append(final-init)

	except PermissionError:
		print(f"Unable to move `{f}` due to permissions error.")
		Path(destination).unlink(missing_ok=True)


print(f"Finished moving {file_count} file(s) in {sum(times):.2f} s.")

if file_count > 1:
	mean_ = mean(times)
	median_ = median(times)
	stdev_ = stdev(times)
	cv = stdev_ / mean_
	print(f"{cv=}")
	if cv >= 1:
		print(f"Average file moving duration: {median_:.2f} ± {stdev_:.2f} s.")
	else:
		print(f"Average file moving duration: {mean_:.2f} ± {stdev_:.2f} s.")

