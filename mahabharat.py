from concurrent.futures import ProcessPoolExecutor
from pathlib import Path
from shutil import unpack_archive


if __name__ == "__main__":
    downloads = Path.home() / "Downloads"
    mahabharat_folder = Path.home() / "Videos" / "Mahabharat"
    mahabharat_zips = (
        i
        for i in downloads.iterdir()
        if i.name.lower().startswith("season") and i.name.endswith(".zip")
    )

    def extract(zpf: Path):
        print(f"Unpacking {zpf.name}")
        unpack_archive(str(zpf), mahabharat_folder, "zip")

    with ProcessPoolExecutor() as pool:
        pool.map(extract, mahabharat_zips)
