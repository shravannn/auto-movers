# TODO
# *MOVE FILES ACCORDING TO THEIR TYPES
# *FOR EX - IF THEY ARE IMAGES THEN THEY SHOULD BE SAVED TO AN ANOTHER FOLDER, IF THEY ARE PROGRAMS THEN TO PROGRAMS FOLDER....

import os, shutil

downloads_dir = os.path.join(os.path.expanduser("~"), "Downloads")
saved_pictures_dir = os.path.join(os.path.expanduser("~"), "Pictures", "Saved Pictures")
os.chdir(downloads_dir)
files_moved = 0

programs = ["exe", "msi"]
compressed_files = ["rar", "zip", "7z"]
images = ["jpg", "jpeg", "png", "jfif"]
videos = ["mp4", "webm"]
music = ["mp3", "ogg", "aac"]
folders = ["Programs", "IDEs", "Compressed files", "PDFs", "Misc"]

for files in os.listdir():
    try:
        if files in folders:
            continue

        ext = files.split(".")[-1].lower()
        if ext in programs:
            path = (os.path.join(downloads_dir, files))
            destination = os.path.join(downloads_dir, "Programs")
            shutil.move(path, destination)
            print(files, "file moved to", destination)
            files_moved += 1

        elif ext in compressed_files:
            path = os.path.join(downloads_dir, files)
            destination = os.path.join(downloads_dir, "Compressed Files")
            shutil.move(path, destination)
            print(files, "file moved to", destination)
            files_moved += 1

        elif ext in images:
            path = os.path.join(downloads_dir, files)
            shutil.move(path, saved_pictures_dir)
            print(files, "file moved to", saved_pictures_dir)
            files_moved += 1

        elif ext == "pdf":
            path = os.path.join(downloads_dir, files)
            destination = os.path.join(downloads_dir, "PDFs")
            shutil.move(path, destination)
            print(files, "file moved to", destination)
            files_moved += 1

        elif ext in videos:
            path = os.path.join(downloads_dir, files)
            destination = os.path.join(os.path.expanduser("~"), "Videos")
            shutil.move(path, destination)
            print(files, "file moved to", destination)
            files_moved += 1

        elif ext in music:
            path = os.path.join(downloads_dir, files)
            destination = os.path.join(os.path.expanduser("~"), "Music")
            shutil.move(path, destination)
            print(files, "file moved to", destination)
            files_moved += 1

        else:
            path = os.path.join(downloads_dir, files)
            destination = os.path.join(os.path.expanduser("~"), "Others")
            shutil.move(path, destination)
            print(files, "file moved to", destination)
            files_moved += 1

    except Exception as e:
        print(e)

else:
    print(f"{files_moved} files successfully moved!")
