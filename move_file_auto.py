"AUTOMATICALLY TRANSFER SCREENSHOTS FROM ITS DIRECTORY TO STUDY CONTENT FOLDER"
import os, time, shutil

def automatically_move(lecture):
    """
    Moves the screenshots to provided lecture's directory.
    """

    screenshot_dir = r"C:\Users\Lenovo\Downloads"
    study_content_dir = r"D:\Study Content"
    os.chdir(screenshot_dir)

    # initialising a list of contents in the directory 
    original_content = os.listdir()

    # dictionary containing all directories according to the lecture
    folders_subject = {1:study_content_dir+"\Biology", 
                       2:study_content_dir+"\Chemistry", 3:study_content_dir+"\Computer", 4:study_content_dir+"\English", 5:study_content_dir+"\Hindi", 6:study_content_dir+"\Maths", 7:study_content_dir+"\Physics", 8:study_content_dir+"\SS", 9:r"C:\Users\Lenovo\Documents\Golang Notes"}

    change = False

    while True:
        try:
            # intialising a new list of directory contents to look for changes
            altered_content = os.listdir()

            # checking if there's a change in list of contents
            if len(altered_content) == len(original_content):
                time.sleep(10)
            else:
                change = True

            if change:
                # identifying the new file to move
                for files in os.listdir(screenshot_dir):
                    if files not in original_content:
                        shutil.move(screenshot_dir + f"\{files}", folders_subject.get(lecture))
                        print(f"One screenshot moved to {folders_subject.get(lecture)} folder!")

        except Exception as e:
            print(e)


if __name__ == "__main__":
    lecture = int(input("Which lecture is it?\n"
                        "1. Biology\n"
                        "2. Chemistry\n"
                        "3. Computer\n"
                        "4. English\n"
                        "5. Hindi\n"
                        "6. Maths\n"
                        "7. Physics\n"
                        "8. SS\n"
                        "9. Golang Notes\n"))

    print("Program has started running... Happy screenshotting :)!")
    automatically_move(lecture)