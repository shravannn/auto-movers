from pathlib import Path
import shutil


def sanitise_filename(text: str) -> str:
    # text = "[S2-03] Kaguya sama [1080p] [Dual] @Anime_Ocean.mkv"
    initials = text.split("Kaguya")[0][1:-2]
    season = initials.split("-")[0]
    episode = initials.split("-")[1]
    ext = text.split(".")[-1]

    return f"{season} E{episode}.{ext}"


def move_file(filepath: Path) -> None:
    base_dest = Path("E:") / "Kaguya Sama Love is War"
    if not base_dest.exists():
        base_dest.mkdir()

    sanitised_filename = sanitise_filename(filepath.name)
    season_number = sanitised_filename.split(" ")[0]

    destination = base_dest / season_number
    if not destination.exists():
        destination.mkdir()

    old_filepath = destination / filepath.name
    new_filepath = destination / sanitised_filename

    try:
        shutil.move(filepath, destination)
        old_filepath.rename(new_filepath)

    except PermissionError:
        new_filepath.unlink()
    except FileNotFoundError:
        print("source file doesnt exists")
    except FileExistsError:
        print("destination file already exists")


if __name__ == "__main__":
    source = Path.home() / "Downloads" / "Telegram Desktop"
    for file in source.iterdir():
        if "Kaguya" in file.name:
            print(f"Moving {file.name}")
            move_file(file)

